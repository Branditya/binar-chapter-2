package id.branditya.chapter2binar

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        openActivityOne()
        openActivityTwo()
    }

    private fun openActivityOne(){
        val buttonOne= findViewById<Button>(R.id.btn_page_one)
        buttonOne.setOnClickListener{
            val intent = Intent (this, ActivityPage1::class.java)
            startActivity(intent)
        }
    }
    private fun openActivityTwo(){
        val buttonOne= findViewById<Button>(R.id.btn_page_two)
        buttonOne.setOnClickListener{
            val intent = Intent (this, ActivityPage2::class.java)
            startActivity(intent)
        }
    }
}